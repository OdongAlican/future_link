import state  from '../../src/store/modules/invoices.js';

describe('invoices actions', () => {

  it('tests fetching invoices with a mock commit', () => {
    let invoices    
    let mockCommit = () => invoices = ['payload']

    state.actions.fetchInvoices({ commit: mockCommit })
       .then(() => { 
           expect(invoices).toEqual(['payload']),
           expect(invoices).not.toEqual(['sample test']) })
  })

//   it('tests posting a invoice with a mock commit ', () => {
//     let invoice
//     let mockCommit = () => invoice = { due_date: "test name",invoice_no: "address" }

//     state.actions.addInvoice({ commit: mockCommit })
//         .then(() => {
//             expect(invoice).toEqual({ due_date: "test name", invoice_no: "address" }),
//             expect(invoice).not.toEqual({ due_date: "test one", invoice_no: "address" })
//         })
//   })
  
})