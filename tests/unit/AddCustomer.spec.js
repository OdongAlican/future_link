import { mount, createLocalVue } from '@vue/test-utils';
import AddCustomer from '../../src/components/AddCustomer';
import Vuelidate from 'vuelidate';

const localVue = createLocalVue()
localVue.use(Vuelidate)

test('Mount AddCustomer', () => {
  const wrapper = mount(AddCustomer, {
    localVue,
    data() {
      return { name: "sample name" }
    }
  })
  expect(wrapper.html()).toContain('Add Customer');
  expect(wrapper.html()).not.toContain('Adds Customer');
})

test('Make a snapshot of AddCustomer', () => {
  const wrapper = mount(AddCustomer, {
    localVue,
    data() {
      return { name: "sample name" }
    }
  })
  
  expect(wrapper.element).toMatchSnapshot()
})