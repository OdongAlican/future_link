import { mount, createLocalVue } from '@vue/test-utils';
import UpdateProduct from '../../src/components/Products/UpdateProduct';
import Vuelidate from 'vuelidate';

const localVue = createLocalVue()
localVue.use(Vuelidate)

test('Mount UpdateProduct', () => {
  const wrapper = mount(UpdateProduct, {
    localVue,
    propsData: {
        product: {
            product_name: "test"
        }
    },
    data() {
      return { product_name: "sample name" }
    }
  })
  expect(wrapper.html()).toContain("Update");
  expect(wrapper.html()).not.toContain('Adds Customer');
})

test('Make a snapshot of UpdateProduct', () => {
  const wrapper = mount(UpdateProduct, {
    localVue,
    propsData: {
        product: {
            product_name: "test"
        }
    },
    data() {
      return { product_name: "sample name" }
    }
  })

  expect(wrapper.element).toMatchSnapshot()
})