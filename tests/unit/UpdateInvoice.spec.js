import { mount, createLocalVue } from '@vue/test-utils';
import UpdateInvoice from '../../src/components/Invoices/UpdateInvoice';
import Vuelidate from 'vuelidate';

const localVue = createLocalVue()
localVue.use(Vuelidate)

test('Mount UpdateInvoice', () => {
  const wrapper = mount(UpdateInvoice, {
    localVue,
    propsData: {
        invoice: { due_date: "12/12/2020" }
    },
    data() {
      return { due_date: "12/12/2020" }
    }
  })
  expect(wrapper.html()).toContain("Update");
  expect(wrapper.html()).not.toContain('Adds Customer');
})

test('Make a snapshot of UpdateInvoice', () => {
  const wrapper = mount(UpdateInvoice, {
    localVue,
    propsData: {
        invoice: { due_date: "12/12/2020" }
    },
    data() {
      return { due_date: "12/12/2020" }
    }
  })

  expect(wrapper.element).toMatchSnapshot()
})