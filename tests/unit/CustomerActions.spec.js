import state  from '../../src/store/modules/customers.js';

describe('customers actions', () => {

  it('tests fetching customers with a mock commit', () => {
    let customers    
    let mockCommit = () => customers = ['payload']

    state.actions.fetchCustomers({ commit: mockCommit })
       .then(() => { 
           expect(customers).toEqual(['payload']),
           expect(customers).not.toEqual(['sample test']) })
  })

  it('tests posting a customer with a mock commit ', () => {
    let customer
    let mockCommit = () => customer = { name: "test name",address: "address" }

    state.actions.addCustomers({ commit: mockCommit })
        .then(() => {
            expect(customer).toEqual({ name: "test name", address: "address" }),
            expect(customer).not.toEqual({ name: "test one", address: "address" })
        })
  })
  
})