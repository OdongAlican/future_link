import state  from '../../src/store/modules/products';

describe('products actions', () => {

  it('tests fetching products with a mock commit', () => {
    let products    
    let mockCommit = () => products = ['payload']

    state.actions.fetchProducts({ commit: mockCommit })
       .then(() => { 
           expect(products).toEqual(['payload']),
           expect(products).not.toEqual(['sample test']) })
  })

  it('tests posting a product with a mock commit ', () => {
    let product
    let mockCommit = () => product = { product_name: "test name",product_price: "address" }

    state.actions.addProduct({ commit: mockCommit })
        .then(() => {
            expect(product).toEqual({ product_name: "test name", product_price: "address" }),
            expect(product).not.toEqual({ product_name: "test one", product_price: "address" })
        })
  })
  
})