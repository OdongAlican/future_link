import { mount, createLocalVue } from '@vue/test-utils';
import UpdateCustomer from '../../src/components/UpdateCustomer';
import Vuelidate from 'vuelidate';

const localVue = createLocalVue()
localVue.use(Vuelidate)

test('Mount UpdateCustomer', () => {
  const wrapper = mount(UpdateCustomer, {
    localVue,
    propsData: {
        customer: { name: "test" }
    },
    data() { return { name: "sample name" }}
  })
  expect(wrapper.html()).toContain("Update");
  expect(wrapper.html()).not.toContain('Adds Customer');
})

test('Make a snapshot of UpdateCustomer', () => {
  const wrapper = mount(UpdateCustomer, {
    localVue,
    propsData: { customer: { name: "test" }
    },
    data() { return { name: "sample name" } }
  })

  expect(wrapper.element).toMatchSnapshot()
})