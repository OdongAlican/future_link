import { mount, createLocalVue } from '@vue/test-utils';
import Products from '../../src/components/Products/Products';
import Vuelidate from 'vuelidate';
import Vuex from 'vuex';

const localVue = createLocalVue()
localVue.use(Vuelidate)
localVue.use(Vuex)

let getters = { allProducts: () => ["first", "second", "third"] }
let actions = { fetchProducts: () => ['test1', 'test2'] }

let store = new Vuex.Store({ getters, actions })

test('Mount Products', () => {
  const wrapper = mount(Products, {
    store,
    localVue,
    stubs: ['jw-pagination']
  })
  expect(wrapper.html()).toContain("Products");
  expect(wrapper.html()).not.toContain('Any Random text');
})

test('Make a snapshot of Products', () => {
  const wrapper = mount(Products, {
    store,
    localVue,
    stubs: ['jw-pagination']
  })

  expect(wrapper.element).toMatchSnapshot()
})

