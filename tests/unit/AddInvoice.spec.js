import { mount, createLocalVue } from '@vue/test-utils';
import AddInvoice from '../../src/components/Invoices/AddInvoice';
import Vuelidate from 'vuelidate';

const localVue = createLocalVue()
localVue.use(Vuelidate)

test('Mount AddInvoice', () => {
  const wrapper = mount(AddInvoice, {
    localVue,
    data() {
      return { due_date: "sample name" }
    },
  })
  expect(wrapper.html()).toContain("Add New Invoice");
  expect(wrapper.html()).not.toContain('Any Random text');
})

test('Make a snapshot of AddInvoice', () => {
  const wrapper = mount(AddInvoice, {
    localVue,
    data() {
      return { due_date: "sample name" }
    },
  })

  expect(wrapper.element).toMatchSnapshot()
})