import { mount, createLocalVue } from '@vue/test-utils';
import AddProduct from '../../src/components/Products/AddProduct';
import Vuelidate from 'vuelidate';

const localVue = createLocalVue()
localVue.use(Vuelidate)

test('Mount AddProduct', () => {
  const wrapper = mount(AddProduct, {
    localVue,
    data() {
      return { product_name: "sample name" }
    },
  })
  expect(wrapper.html()).toContain("Add Product");
  expect(wrapper.html()).not.toContain('Any Random text');
})

test('Make a snapshot of AddProduct', () => {
  const wrapper = mount(AddProduct, {
    localVue,
    data() {
      return { product_name: "sample name" }
    },
  })

  expect(wrapper.element).toMatchSnapshot()
})