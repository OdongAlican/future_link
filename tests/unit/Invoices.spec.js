import { mount, createLocalVue } from '@vue/test-utils';
import Invoices from '../../src/components/Invoices/Invoices';
import Vuelidate from 'vuelidate';
import Vuex from 'vuex'


const localVue = createLocalVue()
localVue.use(Vuelidate)
localVue.use(Vuex)

const $route = { path: '/some/path', params: {  id: "1" } }
let getters = { allInvoices: () => ["first", "second", "third"] }
let actions = { fetchInvoices: () => ['test1', 'test2'] }

let store = new Vuex.Store({ getters, actions })

test('Mount Invoices', () => {
  const wrapper = mount(Invoices, {
    store,
    localVue,
    mocks: {
        $route
    },
    stubs: ['jw-pagination', 'download-csv']
  })
  expect(wrapper.html()).toContain("Download Data");
  expect(wrapper.html()).not.toContain('Any Random text');
})

test('Make a snapshot of Invoices', () => {
  const wrapper = mount(Invoices, {
    store,
    localVue,
    mocks: {
        $route
    },
    stubs: ['jw-pagination', 'download-csv']
  })

  expect(wrapper.element).toMatchSnapshot()
})

