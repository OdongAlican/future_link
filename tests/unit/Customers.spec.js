import { mount, createLocalVue } from '@vue/test-utils';
import Customers from '../../src/components/Customers';
import Vuelidate from 'vuelidate';
import Vuex from 'vuex'


const localVue = createLocalVue()
localVue.use(Vuelidate)
localVue.use(Vuex)

let getters = { allCustomers: () => ["first", "second", "third"] }
let actions = { fetchCustomers: () => ['test1', 'test2'] }

let store = new Vuex.Store({ getters, actions })

test('Mount Customers', () => {
  const wrapper = mount(Customers, {
    store,
    localVue,
    stubs: ['router-link']
  })
  expect(wrapper.html()).toContain("Customers");
  expect(wrapper.html()).not.toContain('Any Random text');
})

test('Make a snapshot of Customers', () => {
  const wrapper = mount(Customers, {
    store,
    localVue,
    stubs: ['router-link']
  })

  expect(wrapper.element).toMatchSnapshot()
})
