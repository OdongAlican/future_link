# This project was bootstrapped with [vue create my-project](https://cli.vuejs.org/).

# Built with

- Vue
- Vuex
- Jest
- Vue-test-utils
- Bootstrap

# Invoices Application

- This application displays the list of customers and invoices under a single customer. A user can also view a list of products. Users have the ability to edit, delete and create a customer, an invoice under a customer and a product.
A user can also export the csv file of all the invoices.


## App Images
### Customers
![First Image](public/customers.png)

### Invoices
![Second Image](public/Invoices.png)

### Products
![Second Image](public/Products.png)

# Project Setup
- Fork the Project to your remote repository
- Clone the Project to your local machine
- Run `npm install` to install project dependencies

## Available Scripts

In the project directory, you can run:

### `npm run serve`

Runs the app in the development mode.<br />
Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm run test:unit`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://vue-test-utils.vuejs.org/) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles Vue in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://cli.vuejs.org/guide/deployment.html#github-pages) for more information.

## Author

👤 **Odong Sunday**

- [Portfolio](https://portfolio-pink-mu.vercel.app/)
- Github: [@OdongAlican](https://github.com/OdongAlican)
- Gitlab: [@OdongAlican](https://gitlab.com/OdongAlican)
- Twitter: [@odongsandie](https://twitter.com/odongsandie)
- Linkedin: [Sunday Alican odong](https://www.linkedin.com/in/sunday-alican-odong/)
- [Email](mailto:sandieo.2020@gmail.com)


## 🤝 Contributing

Contributions, issues and feature requests are welcome!

Feel free to check the [issues page](https://gitlab.com/OdongAlican/future_link/-/issues).

## Show your support

Give a ⭐️ if you like this project!